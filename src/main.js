import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';
import Component from './components/Component';
import PizzaForm from './pages/PizzaForm';
Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList([]),
    aboutPage = new Component('section', null, 'Ce site est génial'),
	pizzaForm = new PizzaForm();

Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' },
                 { path: '/a-propos', page: aboutPage, title: 'À propos' },
                 { path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];

const link = document.querySelector('button'); // sélectionne le premier lien de la page

link.addEventListener('click', event => {
    event.preventDefault();
    document.querySelector('section').setAttribute('style', 'display:none');
    console.log(event);
}); 
Router.navigate('/'); // affiche une page vide
pizzaList.pizzas = data;
Router.navigate('/'); // affiche la liste des pizzas
document.querySelector('section').setAttribute('style','');
Router.menuElement = document.querySelector('.mainMenu');

