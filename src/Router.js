export default class Router {
	static titleElement;
	static contentElement;
	static routes = [];
	
	static #menuElement;
	static set menuElement(element) {
        this.#menuElement = element;
        console.log(`${this.#menuElement.querySelectorAll('a')} a`);
        const tab = this.#menuElement.querySelectorAll('a');
        tab.forEach(link => {
            link.addEventListener('click', event => {
                event.preventDefault();
                Router.navigate(link.getAttribute('href'));
            });
        });
    }
    
	
	static navigate(path) {
		const route = this.routes.find(route => route.path === path);
		if (route) {
			this.titleElement.innerHTML = `<h1>${route.title}</h1>`;
			this.contentElement.innerHTML = route.page.render();
			route.page.mount?.(this.contentElement);

		}
	}
}
